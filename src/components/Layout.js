import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Popup from "reactjs-popup"
import HOC from '../HOC';
import Header from './template/Header';
import SideBar from './template/SideBar';
import Dashboard from './dashboard/Dashboard';
import Users from './users/Users';
import Profile from './profile/Profile';
import Answers from './answers/Answers';
import EditProfile from './profile/EditProfile';
import ChangePassword from './forgetPassword/ChangePassword';
import Device from './device/Device';
import EditDevice from './device/EditDevice';
import Exercise from './exercise/Exercise';
import Coupons from './coupons/Coupons';
import Questionaire from './questionaire/Questionaire';
import { checkSession } from '../config/services/usersService';
import { logout } from '../config/services/adminService';
import sessionExpireIcon from "../assets/images/sessionExpired.png"
import Products from './products/Products';
import EditProduct from './products/EditProduct';
import UserChart from './users/UsersChart';
import Dieticians from './dietician/Dieticians';
import Foods from './foodItems/foodList'
import FoodCategory from './foodItems/foodCategories'
import ExerciseCategory from './exerciseCategory/exerciseCategory'
import { toast } from 'react-toastify';


class Layout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sessionExpired: false,
        }

        this.handlelogOut = this.handlelogOut.bind(this);
    }

    componentWillMount() {
        if (localStorage.getItem('accessToken') === undefined || localStorage.getItem('accessToken') === null) {
            this.props.history.push('/login')
            return;
        } else {
            this.checkSessionFunction();
        }
    }

    checkSessionFunction(accessToken) {
        checkSession()
            .then(res => {
                if (res.data.statusCode === 0) {
                    this.setState({ sessionExpired: true })
                } else {
                    this.setState({ sessionExpired: false })
                }
            })
    }

    sessionExpired = () => {
        this.setState({ sessionExpired: false })
        localStorage.clear();
        this.props.history.push("/login")
    }



    handlelogOut() {
        logout()
            .then(res => {
                if (res.data.statusCode == 1) {
                    localStorage.clear();
                    localStorage.clear();
                    this.props.history.push('/login')
                } else if (res.data.statusCode == 0) {
                    toast.error(res.data.error.errorMessage);
                }
            })
    }

    render() {
        return (
            <HOC>
                <Header />
                <SideBar handlelogOut={this.handlelogOut} />
                <Switch>
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/users" component={Users} />
                    <Route path="/profile/:id" component={Profile} />
                    <Route path="/answers/:id" component={Answers} />
                    <Route path="/edit-profile" component={EditProfile} />
                    <Route path="/change-password" component={ChangePassword} />
                    <Route path="/device" component={Device} />
                    <Route path="/edit-device/:id" component={EditDevice} />
                    <Route path="/coupons" component={Coupons} />
                    <Route path="/products" component={Products} />
                    <Route path="/edit-product/:id" component={EditProduct} />
                    <Route path="/chart/:id" component={UserChart} />
                    <Route path="/dieticians" component={Dieticians} />
                    <Route path="/food-category" component={FoodCategory} />
                    <Route path="/foods" component={Foods} />
                    <Route path="/questionaire" component={Questionaire} />
                    <Route exact path="/exercise-category" component={ExerciseCategory} />
                    <Route exact path="/exercise" component={Exercise} />


                    <Redirect from="/" to="/dashboard" />
                </Switch>

                <Popup
                    open={this.state.sessionExpired}
                    closeOnDocumentClick={false}
                >
                    <div className="session_expired cm_modal text-center">
                        <img className="sessionIcon mb-4" src={sessionExpireIcon} />
                        <h4 className="text-center">Oops!! Please Login again</h4>
                        <button onClick={this.sessionExpired} className="btn btn-primary btn-block">Login</button>
                    </div>
                </Popup>
            </HOC>
        )
    }
}


export default Layout;