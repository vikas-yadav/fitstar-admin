import React, { Component } from 'react';
import dateFormat from 'dateformat';
import Pagination from "react-js-pagination";
import LaddaButton, { EX, SLIDE_UP } from 'react-ladda';
import HOC from '../../HOC';
import { Link } from 'react-router-dom';
import Popup from "reactjs-popup";
import { getUserList, userChangeStatus, getTotalUsersCount } from '../../config/services/usersService';
import { DieticianService } from '../../config/services/dieticianService';

import { LoaderSvg } from '../Reusable'
import { toast } from 'react-toastify';

class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNo: 1,
      activePage: 0,
      users: [],
      Loader: true,
      UserSearch: '',
      sortFieldValue: '',
      FilterStatus: false,
      sortField: 'created',
      sortFieldIcon: false,
      sortType: 2,
      userId: '',
      userActiveStatus: '',
      loading: false,
      totalUserCount: 0,
      filterUsers: '',
      assignDieticianPopup: false,
      dieticiansList: [],
      selectedDietician: '',
      assignToUserId: '',
      validationError:'',
    }

    this.getUserList = this.getUserList.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleUserSearch = this.handleUserSearch.bind(this);
    this.handleSortList = this.handleSortList.bind(this);
    this.toggleFilterStatus = this.toggleFilterStatus.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }


  openAssignDieticianPopup = (userData, event) => {
    this.setState({ assignDieticianPopup: !this.state.assignDieticianPopup, assignToUserId: userData._id })
  }

  closeModal = () => this.setState({ assignDieticianPopup: false, selectedDietician: '', assignToUserId: '' })


  handleClickOutside(event) {
    if (event.target.id !== 'filter_text') {
      this.setState({
        FilterStatus: false
      })
    } else {
      this.setState({
        FilterStatus: true
      })
    }
  }


  getDieticiansList() {

    let self = this;

    DieticianService.getAllDieticians()
      .then(function (res) {
        const response = res.data;
        if (response.statusCode == 1) {
          const { dieticiansList } = res.data.responseData
          self.setState({ dieticiansList })
        } else if (response.statusCode == 0) {
          toast.error(response.error.errorMessage)
        }
      })
  }



  toggleFilterStatus() {
    this.setState({
      FilterStatus: !this.state.FilterStatus
    })
    document.addEventListener('click', this.handleClickOutside);
  }



  getUserList() {
    let params = `?pageNo=${this.state.pageNo - 1}&count=20&search=${this.state.UserSearch}&sortField=${this.state.sortField}&sortType=${this.state.sortType}&filterUsers=${this.state.filterUsers}`;
    getUserList(params)
      .then(res => {
        console.log('respons', res)
        if (res.data.statusCode === 1) {
          this.setState({
            users: res.data.responseData,
            activePage: res.data.responseData.length,
            Loader: false
          })
        }
      })
  }



  handlePageChange(pageNumber) {

    this.setState({ pageNo: pageNumber, Loader: true, UserSearch: '' }, () => {
      this.getUserList();
    });

  }





  handleUserSearch(e) {
    this.setState({ UserSearch: e.target.value }, () => {
      this.getUserList();
    });
  }



  handleSortList(sortField) {
    const { sortType } = this.state;

    let sortOrder = sortType === 1 ? 2 : 1

    this.setState({ sortField: sortField, Loader: true, sortType: sortOrder }, () => {
      this.getUserList();
      this.setState({
        sortFieldIcon: !this.state.sortFieldIcon
      })
    });
  }


  componentDidMount() {

    let token = localStorage.getItem('accessToken');

    if (token) {
      this.getUserList();
    }

    this.getDieticiansList();

    getTotalUsersCount()
      .then(res => {
        if (res.data.statusCode === 1) {
          this.setState({ totalUserCount: res.data.responseData.totalUsers })
        }
      })
  }


  handleChangeStatus(id, status) {
    this.setState({ [id]: true, progress: 0.5, })

    let self = this;
    let obj = {
      activeUserId: id,
      activeStatus: status === 1 ? 2 : 1
    }
    userChangeStatus(obj)
      .then((res) => {
        if (res.data.statusCode === 1) {
          self.getUserList();
          self.setState({ [id]: false })
        }
      })
  }

  hendleSortUsers = query => {
    this.setState({ Loader: true, filterUsers: query }, () => this.getUserList());
  }

  onDieticianSelect = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value })
  }

  onAssignDieticianSubmit = (e) => {
    e.preventDefault();

    const { selectedDietician, assignToUserId } = this.state

    let self = this;
    let params = {
      dieticianId: selectedDietician,
      assignToUserId
    }

    if (!selectedDietician) {
      this.setState({validationError:"Please select a dietician"})
    } else {
      this.setState({validationError:""})
      DieticianService.assignDietician(params)
        .then(function (res) {
          let response = res.data;
          if (response.statusCode == 1) {
            self.closeModal();
            toast.success(response.responseData.message)
            self.getUserList();
          } else if (response.statusCode == 0) {
            toast.error(response.error.errorMessage)
          }
        })
    }

  }

  render() {
    const { pageNo, dieticiansList,validationError } = this.state;
    return (
      <HOC>
        <div className="body-container-wrapper">
          <div className="body-container">
            <ol className="breadcrumb">
              <li className="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
              <li className="breadcrumb-item">Users</li>
            </ol>


            {this.state.Loader
              ? <div className="loader_wrapper"><LoaderSvg /></div>
              : null}

            <div className="users_header">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-9">
                    <h4 className="cm_page_heading">Users</h4>
                  </div>
                  <div className="col-md-3 text-right cm_search">
                    <input
                      type="text"
                      className="form-control m-b-md-15"
                      placeholder="Search by Name or E-mail"
                      onChange={this.handleUserSearch}
                      value={this.state.UserSearch}
                    />
                    <i className="fa fa-search" ></i>
                  </div>
                </div>
              </div>
            </div>
            <div className="table-responsive">
              <table className="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col" className="filter_text">Name <i className={`fa fa-sort-alpha-${this.state.sortFieldIcon ? 'desc' : 'asc'}`}></i><button onClick={() => this.handleSortList('firstName')} value="filterFirstName"></button></th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Assigned Dietician</th>
                    <th scope="col" className="filter_text">Email <i className={`fa fa-sort-alpha-${this.state.sortFieldIcon ? 'desc' : 'asc'}`} ></i><button onClick={() => this.handleSortList('email')} value="filterEmail"></button></th>
                    <th scope="col">Gender</th>

                    <th scope="col">Request for dietician</th>

                    <th scope="col" id="filter_text" className={this.state.FilterStatus ? 'filter_text open' : 'filter_text'} onClick={this.toggleFilterStatus}>
                      Status <i className="fa fa-filter" ></i>
                      <ul>
                        <li onClick={() => this.hendleSortUsers(1)}>Active User</li>
                        <li onClick={() => this.hendleSortUsers(2)}>Inactive User</li>
                        <li onClick={() => this.hendleSortUsers('')}>All User</li>
                      </ul>
                    </th>
                  </tr>
                </thead>


                <tbody>
                  {
                    this.state.users ? (
                      this.state.users.map((user, index) => {
                        return (
                          <tr key={user._id}>
                            <td>{((index + 1) + ((pageNo - 1) * 20))}</td>
                            <td className="cm_pos_realtive">
                              {`${user.firstName} ${user.lastName}`}
                              <Link to={'/profile/' + user._id} className="btn btn-info btn-sm">View</Link>
                            </td>
                            <td>{`+${user.countryCode} - ${user.mobileNo}`}</td>

                            <td>{user.assignedDietician?user.assignedDietician.firstName +" "+ user.assignedDietician.lastName:'Not yet assigned'}</td>
                            
                            <td>{user.email}</td>
                            <td>{user.gender === 1 ? 'Male' : 'Female'}</td>

                            <td>
                              {user.isDieticianAssigned ? <button className="btn btn-info btn-sm cm__btn ml-3" onClick={this.openAssignDieticianPopup.bind(this, user)} >Change Dietician</button> : (user.requestForDietician ? <button className='btn btn-info btn-sm' onClick={this.openAssignDieticianPopup.bind(this, user)}> Assign Dietician</button> : 'No Request')}
                            </td>

                            <td style={{ position: 'relative' }}>

                              <LaddaButton
                                loading={this.state[user._id]}
                                onClick={() => this.handleChangeStatus(user._id, user.status)}
                                data-color="red"
                                data-size={EX}
                                data-style={SLIDE_UP}
                                data-spinner-size={45}
                                data-spinner-color="rgb(255,108,11)"
                                data-spinner-lines={20}
                              >
                                <span
                                  className={`cm_ckeckbox_wrapper ${user.status === 1 ? 'cm_active' : 'cm_inactive'}`}
                                  onClick={() => this.handleChangeStatus(user._id, user.status)}
                                >
                                  <span className="cm_ckeckbox_btn"></span>
                                </span>
                              </LaddaButton>
                            </td>
                          </tr>
                        )
                      })
                    ) : (
                        <tr>
                          <td colSpan="8"></td>
                        </tr>
                      )
                  }



                </tbody>


              </table>
              {this.state.users.length === 0 ? <h3 className="empty_error">Sorry, we couldn't find any content for <span>{this.state.UserSearch}</span></h3> : null}
            </div>
            <Pagination
              activePage={this.state.pageNo}
              itemsCountPerPage={20}
              totalItemsCount={this.state.totalUserCount}
              pageRangeDisplayed={3}
              onChange={this.handlePageChange}
            />

          </div>
        </div>




        <Popup
          open={this.state.assignDieticianPopup}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div className="cm_modal pl-5 pr-5">
            <span className="cm_modal_close" onClick={this.closeModal}>
              &times;
            </span>
            <h3 className="text-center mb-4 mt-5">Assign Dietician</h3>
            <div className="text-danger text-center">{validationError}</div>
            <div className="text-danger text-center"></div>

            <form className="mb-5" onSubmit={this.onAssignDieticianSubmit}>

              <select className="form-control" onChange={this.onDieticianSelect} name="selectedDietician">
                <option selected disabled> Select..... </option>
                {dieticiansList.map((dietician, index) => (
                  <option value={dietician._id} > {dietician.firstName + " " + dietician.lastName} </option>
                ))}
              </select>
              <button className="btn btn-primary btn-block mt-4" type="submit">Assign</button>
            </form>

          </div>
        </Popup>

      </HOC>
    );
  }
}

export default Users;