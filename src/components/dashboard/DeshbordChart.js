import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import { Link,withRouter } from 'react-router-dom';
import { getAllDataCount } from '../../config/services/usersService';
import { TreadmillSvg, CouponsSvg, LoaderSvg } from '../Reusable';
import Popup from "reactjs-popup"

class DeshbordChart extends Component {
    constructor(props) {
        super(props);


        this.state = {
            Loader: true,
            AllCountData: {},
            sessionExpired:false,

            data: {
                labels: ['Monday', 'Tuesday', 'Wednesday', 'Thirsday', 'Friday', 'Saturday', 'Sunday'],
                datasets: [
                    {
                        label: 'Chart',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: 'rgba(75,192,192,0)',
                        borderColor: '#20232a',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: 'red',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 20,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        maintainAspectRatio: false,
                        data: [70, 59, 80, 81, 56, 55, 40]
                    }
                ]
            }
        }
    }



    getLabels = (Label) => {
        switch (Label) {
            case 'weeks':
                this.setState({
                    data: {
                        ...this.state.data,
                        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
                        datasets: [{ data: [56, 55, 40, 70] }]
                    }
                })
                break;
            case 'months':
                this.setState({
                    data: {
                        ...this.state.data,
                        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        datasets: [{ data: [70, 59, 80, 81, 56, 55, 40, 70, 59, 80, 81, 56] }]
                    }
                })
                break;
            case 'years':
                this.setState({
                    data: {
                        ...this.state.data,
                        labels: ['2017', '2018', '2019'],
                        datasets: [{ data: [56, 40, 70] }]
                    }
                })
                break;
            case 'days':
                this.setState({
                    data: {
                        ...this.state.data,
                        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thirsday', 'Friday', 'Saturday', 'Sunday'],
                        datasets: [{ data: [70, 59, 80, 81, 56, 55, 40] }]
                    }
                })
                break;
            default:
                this.setState({
                    data: {
                        ...this.state.data,
                        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thirsday', 'Friday', 'Saturday', 'Sunday'],
                        datasets: [{ data: [70, 59, 80, 81, 56, 55, 40] }]
                    }
                })
        }

    }

    componentDidMount() {

        let token = localStorage.getItem('accessToken');

        if (token) {
            getAllDataCount()
                .then(res => {
                    if (res.data.statusCode == 1) {
                        this.setState({ AllCountData: res.data.responseData.totalCounts, Loader: false, })
                    }else{
                        this.setState({Loader: false, })
                    }
                })
        }

    }

    render() {

        let { coupon, device, user } = this.state.AllCountData;
        return (
            <div className="chart_wrapper">
                <div className="row mb-3">
                    <div className="col-lg-4 col-md-6 mb-3">
                        <Link to="/users">
                            <div className="cm_card">
                                <span className="all_users">
                                    <i className="fa fa-users" aria-hidden="true"></i>
                                </span>
                                <h3>Users.</h3>
                                <h2>{user}</h2>
                            </div>
                        </Link>
                    </div>


                    <div className="col-lg-4 col-md-6 mb-3">
                        <Link to="/device">
                            <div className="cm_card">
                                <span className="all_users">
                                    <TreadmillSvg />
                                </span>
                                <h3>Device.</h3>
                                <h2>{device}</h2>
                            </div>
                        </Link>
                    </div>



                    <div className="col-lg-4 col-md-6">
                        <Link to="/coupons">
                            <div className="cm_card">
                                <span className="all_users">
                                    <CouponsSvg />
                                </span>
                                <h3>Coupons.</h3>
                                <h2>{coupon}</h2>
                            </div>
                        </Link>
                    </div>
                </div>

                {this.state.Loader
                    ? <div className="loader_wrapper"><LoaderSvg /></div>
                    : null
                }

            </div>
        )
    }
}

export default DeshbordChart;