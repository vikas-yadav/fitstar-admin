import React, { Component } from 'react';
import dateFormat from 'dateformat';
import Pagination from "react-js-pagination";
import HOC from '../../HOC';
import { ProductsService } from '../../config/services/ProductsService';
import { LoaderSvg } from '../Reusable'
import AmazonIcon from '../../assets/images/Amazon-icon.png'
import FlipkartIcon from '../../assets/images/flipkart-icon.png'

import Popup from "reactjs-popup"
import { Link } from 'react-router-dom';

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNo: 1,
      products: [],
      productSearch: '',
      productName: '',
      productCode: '',
      productLink: '',
      productASIN: '',
      productWebsite: '0',
      productCategory: '',
      productCategoryIndex: '',
      productImagePreview: '',
      productSaveLoader: false,
      productDescription: '',
      productPrice: '',

      totalProducts: '',
      search: '',
      openEditProduct: false,
      productObj: '',
      openAddProductPopup: false,
      activePage: 0,
      Loader: true,
      open: false,
      openDropdown: false,
      name: '',
      uniqueId: '',
      type: null,
      typeIndex: null,
      errors: false,
      validateError: false,
      sortField: 1,
      sortFieldIcon: false,
      sortType: 2,

    }
    this.showProductsList = this.showProductsList.bind(this);
    this.openProductModal = this.openProductModal.bind(this);
    this.closeProductModal = this.closeProductModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addProduct = this.addProduct.bind(this);
    this.handleProductSearch = this.handleProductSearch.bind(this);
    this.handleSortList = this.handleSortList.bind(this);
    this.openEditProductModal = this.openEditProductModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleDeleteProduct = this.handleDeleteProduct.bind(this);
    this.handleValidation = this.handleValidation.bind(this);
    this.getProductsCount = this.getProductsCount.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);

  }

  componentDidMount() {
    this.showProductsList();
    this.getProductsCount();

  }


  handlePageChange(pageNumber) {
    this.setState({ pageNo: pageNumber }, () => {
      this.showProductsList();
    });
  }

  getProductsCount() {
    let self = this;
    ProductsService.getTotalProductsCount()
      .then(function (res) {
        if (res.data.statusCode === 1) {
          self.setState({
            totalProducts: res.data.responseData.count,
          })
        }
      })
  }

  showProductsList() {
    let params = `?pageNo=${this.state.pageNo - 1}&count=20&search=${this.state.search}&sortField=${this.state.sortField}&sortType=${this.state.sortType}`;
    this.setState({Loader:true})

    ProductsService.getProductsList(params)
      .then(res => {
        if (res.data.statusCode === 1) {
          this.setState({
            products: res.data.responseData.products,
            Loader: false
          })
        } else if (res.data.statusCode === 0) {
          this.setState({
            products: [],
            Loader: false
          })
        }
      })
  }

  handleValidation() {
    let { productName,
      productCode,
      productLink,
      productASIN } = this.state

    if (productName || productCode || productLink || productASIN == '') {
      return false
    } else {
      return true
    }
  }

  addProduct(e) {
    e.preventDefault();
    this.setState({ Loader: true, productSaveLoader: true });

    let { productName,
      productCode,
      productLink,
      productASIN, productWebsite, productCategoryIndex, productImage, productDescription, productPrice } = this.state

    let params = new FormData();
    params.append('productName', productName);
    params.append('productCode', productCode);
    params.append('productLink', productLink);
    params.append('productASIN', productASIN);
    params.append('productWebsite', productWebsite);
    params.append('productCategory', productCategoryIndex);
    params.append('productImage', productImage);
    params.append('productDescription', productDescription);
    params.append('productPrice', productPrice);



    ProductsService.addProduct(params)
      .then(res => {

        if (res.data.statusCode === 1) {
          this.setState({
            error: '',
            openAddProductPopup: false,
            Loader: false,
            productSaveLoader: false
          })
          this.showProductsList();
        } else {
          this.setState({
            error: res.data.error.errorMessage,
            productSaveLoader: false,
          })
        }
      })
  }

  openProductModal() {
    this.setState({ openAddProductPopup: true })
  }
  closeProductModal() {
    this.setState({
      openAddProductPopup: false,
      productName: '',
      productCode: '',
      productLink: '',
      productWebsite: '0',
      productCategory: '',
      productCategoryIndex: '',
      error: '',
      productImagePreview: '',
      productDescription: '',
      productPrice: '',
    })
  }

  handleChange(e) {
    if (e.target.id == 'productImage') {

      let file = e.target.files[0];
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          productImagePreview: reader.result
        });
      }
      reader.readAsDataURL(file)
      this.setState({
        productImagePreview: file
      })


      this.setState({
        productImage: e.target.files[0]
      });
    } else {
      this.setState({
        [e.target.id]: e.target.value,
        errors: false
      })
    }
  }

  handleProductSearch(e) {
    this.setState({ [e.target.id]: e.target.value }, function (search) {
      this.showProductsList();
    })
  }

  handleSortList(sortField) {
    const { sortType } = this.state;

    let sortOrder = sortType === 1 ? 2 : 1;

    this.setState({ sortField: sortField, sortType: sortOrder }, function (sorted) {
      this.showProductsList();
      this.setState({ sortFieldIcon: !this.state.sortFieldIcon })
    })
  }

  openEditProductModal(product) {
    this.setState({
      openEditProduct: true,
      productObj: product._id,
    })
  }

  closeModal() {
    this.setState({ openEditProduct: false })
  }

  handleOpenDropdown = () => {
    this.setState({ openDropdown: !this.state.openDropdown })
  }

  setProductCategory(productCategoryIndex, productCategory) {
    this.setState({ productCategoryIndex, productCategory, openDropdown: false },
      function (res) {
        console.log(this.state.productCategoryIndex, "<<=>>", this.state.productCategory)
      }
    )
  }


  handleDeleteProduct(productInfo) {
    productInfo.preventDefault();
    this.setState({ Loader: true });
    let params = {
      productId: this.state.productObj,
    };
    ProductsService.deleteProduct(params)
      .then(res => {
        if (res.data.statusCode === 1) {
          this.setState({
            openEditProduct: false,
            Loader: false,
            productObj: ''
          })
          this.showProductsList();
        }
      })
  }


  render() {
    const { productSaveLoader,Loader } = this.state;
    return (
      <HOC>
        <div className="body-container-wrapper">
          <div className="body-container">
            <ol className="breadcrumb">
              <li className="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
              <li className="breadcrumb-item">Products</li>
            </ol>


            {
              Loader
                ? <div className="loader_wrapper"><LoaderSvg /></div>
                : null
            }

            <div className="users_header">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-6">
                    <h4 className="cm_page_heading">Products</h4>
                  </div>
                  <div className="col-md-3 text-right cm_search">
                    <input
                      type="text"
                      className="form-control m-b-md-15"
                      placeholder="Search"
                      id="search"
                      onChange={this.handleProductSearch}
                    />
                    <i className="fa fa-search"></i>
                  </div>

                  <div className="col-md-3  m-b-md-15">
                    <button onClick={this.openProductModal} className="btn btn-primary btn-block">Add Product</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="table-responsive">
              <table className="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col" className="filter_text">Name <i className={`fa fa-sort-alpha-${this.state.sortFieldIcon ? 'desc' : 'asc'}`}></i><button onClick={() => this.handleSortList(0)}></button></th>
                    <th>Product Code</th>
                    <th>Product ASIN</th>
                    <th>Product Link</th>
                    <th>Product Site</th>

                    <th scope="col" className="filter_text">Created Date <i className={`fa fa-sort-numeric-${this.state.sortFieldIcon ? 'desc' : 'asc'}`}></i><button onClick={() => this.handleSortList(1)}></button></th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>


                <tbody>
                  {
                    this.state.products ? (
                      this.state.products.map((product, index) => {
                        return (
                          <tr key={product._id}>
                            <td>{((index + 1) + ((this.state.pageNo - 1) * 20))}</td>
                            <td>{product.productName}</td>
                            <td>{product.productCode}</td>
                            <td>{product.productASIN}</td>
                            <td>{product.productLink}</td>
                            <td>{product.productWebsite == 0 ? <img width="25" src={AmazonIcon} /> : <img width="25" src={FlipkartIcon} />}</td>
                            <td>{dateFormat(product.created, "d mmmm yyyy")}</td>
                            <td><span className="cm_btn_sm" onClick={() => this.openEditProductModal(product)}><i className="fa fa-pencil-square" ></i></span></td>
                          </tr>
                        )
                      })
                    ) : (
                        <tr>
                          <td colSpan="8">{this.state.error}</td>
                        </tr>
                      )
                  }
                </tbody>


              </table>
              {this.state.products.length === 0 ? <h3 className="empty_error">Sorry, We couldn't find any content! <span>{this.state.deviceSearch}</span></h3> : null}
              <Pagination
                activePage={this.state.pageNo}
                itemsCountPerPage={20}
                totalItemsCount={this.state.totalProducts}
                pageRangeDisplayed={3}
                onChange={this.handlePageChange}
              />
            </div>


          </div>
        </div>

        <Popup
          open={this.state.openAddProductPopup}
          closeOnDocumentClick
          onClose={this.closeProductModal}
        >
          <div className="cm_modal pl-5 pr-5">
            <span className="cm_modal_close" onClick={this.closeProductModal}>
              &times;
            </span>
            <h3 className="text-center mb-4 mt-5">Add New Product</h3>
            <h4 className="text-center mb-4 mt-5 error_message">{this.state.error}</h4>

            <form onSubmit={this.addProduct} className="mb-5">

              <div className="form-group">
                <label>Name of Product</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="productName"
                  id="productName"
                />
              </div>

              <div className="form-group">
                <label>Product Code</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="productCode"
                  id="productCode"
                />
              </div>

              <div className="form-group">
                <label>Product ASIN</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="productASIN"
                  id="productASIN"
                />
              </div>

              <div className="form-group">
                <label>Product Link</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="productLink"
                  id="productLink"
                />
              </div>

              <div className="form-group">
                <label>Product Description</label>
                <textarea
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="productDescription"
                  id="productDescription"
                />
              </div>

              <div className="form-group">
                <label>Product Price</label>
                <input
                  type="number"
                  className="form-control"
                  onChange={this.handleChange}
                  name="productPrice"
                  id="productPrice"
                />
              </div>

              <div className={this.state.openDropdown ? 'form-group cm_dropdown open' : 'form-group cm_dropdown'}>
                <label>Product Category</label>
                <span className="form-control cm_blank_input" onClick={this.handleOpenDropdown}>{this.state.productCategory}</span>
                <input type="hidden" value={this.state.productCategory} name="type" />
                <ul>
                  <li onClick={() => this.setProductCategory('0', "Treadmill")}>
                    <h4>Treadmill</h4>
                  </li>
                  <li onClick={() => this.setProductCategory('1', "Incline Trainers")}>
                    <h4>Incline Trainers</h4>
                  </li>
                  <li onClick={() => this.setProductCategory('2', "Ellipticals")}>
                    <h4>Ellipticals</h4>
                  </li>
                  <li onClick={() => this.setProductCategory('3', "Bike")}>
                    <h4>Bike</h4>
                  </li>
                </ul>
              </div>


              <div className="form-group mt-5 mb-5">
                <label className="d-block">Select Product Website</label>
                <div className="radio_wrapper text-center">
                  <label>
                    Amazon
                                            <input type="radio" defaultChecked={true} onChange={this.handleChange} value={0} id="productWebsite" name="productWebsite" />
                    <span></span>
                  </label>
                  <label>
                    Flipkart
                                            <input type="radio" value={1} onChange={this.handleChange} id="productWebsite" name="productWebsite" />
                    <span></span>
                  </label>
                </div>
              </div>


              <div className="form-group mt-5 mb-5">
                <label className="d-block">Upload Product Image</label>
                <div className="radio_wrapper text-center">
                  <input type="file" onChange={this.handleChange} id="productImage" name="productImage" />
                  <span></span>
                  <img width="400px" src={this.state.productImagePreview} />
                </div>
              </div>



              <button className="btn btn-primary btn-block mt-4" disabled={productSaveLoader}>
                {!productSaveLoader ? <span>Save Product</span> :
                  <div>
                    <i className="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i>
                    <span>Saving Product....</span>
                  </div>
                }

              </button>
              {this.state.validateError ? <span className="error">error</span> : null}

            </form>
            {
              this.state.errors
                ? <div className="loader_wrapper"><LoaderSvg /></div>
                : null
            }
          </div>
        </Popup>

        <Popup
          open={this.state.openEditProduct}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div className="cm_modal">
            <span className="cm_modal_close" onClick={this.closeModal}>
              &times;
            </span>
            <h3 className="text-center mb-4">Product Action</h3>
            <div className="btn_group">
              <div className="row">
                <div className="col"><Link to={'/edit-product/' + this.state.productObj} className="btn btn-warning btn-sm btn-block">Edit</Link></div>
                <div className="col"><button className="btn btn-danger btn-sm btn-block" onClick={this.handleDeleteProduct}>Delete</button></div>
              </div>
            </div>
          </div>
        </Popup>


      </HOC>



    );
  }
}

export default Products;