import React, { Component } from 'react';
import HOC from '../../HOC';
import { ProductsService } from '../../config/services/ProductsService';
import { LoaderSvg } from '../Reusable';
import { Link } from 'react-router-dom';

class EditProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            productName: '',
            productCode: '',
            productLink: '',
            productASIN: '',
            productDescription: '',
            productPrice: '',
            productImage: '',
            productImagePreview: '',
            updateError:'',
            Loader: false,
            productUpdateLoader:false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.editProduct = this.editProduct.bind(this)
    }

    productDetails() {
        let params = `?productId=${this.props.match.params.id}`;
        console.log(params)
        ProductsService.getProduct(params)
            .then((res) => {
                if (res.data.statusCode === 1) {
                    let { productName, productCode, productASIN, productLink, productDescription, productPrice, productImage } = res.data.responseData.product;
                    this.setState({
                        productName, productCode, productASIN, productLink, productDescription, productPrice, productImage,
                        Loader: false,
                    })
                }
            })
    }

    handleChange(e) {

        if (e.target.id == 'productImage') {
            let file = e.target.files[0];
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    productImagePreview: reader.result
                });
            }
            reader.readAsDataURL(file)
            this.setState({
                productImagePreview: file
            })
            this.setState({
                productImage: e.target.files[0]
            });
        } else {
            let { id, value } = e.target;
            this.setState({ [id]: value })
        }
    }


    componentDidMount() {
        this.productDetails();
    }

    editProduct(e) {
        e.preventDefault();

        let { productName, productCode, productASIN, productLink, productDescription, productImage, productPrice } = this.state;
        // let obj = {
        //     productId: this.props.match.params.id,
        //     productName: productName,
        //     productCode: productCode,
        //     productASIN: productASIN,
        //     productLink: productLink,
        // };

        let obj = new FormData();
        obj.append('productId', this.props.match.params.id);
        obj.append('productName', productName);
        obj.append('productCode', productCode);
        obj.append('productASIN', productASIN);
        obj.append('productLink', productLink);
        obj.append('productDescription', productDescription);
        obj.append('productImage', productImage);
        obj.append('productPrice', productPrice);


        this.setState({productUpdateLoader:true, updateError:''})
        ProductsService.editProduct(obj)
            .then(res => {
                if (res.data.statusCode === 1) {
                    this.setState({
                        errors: false,
                        updateError:'',
                        productUpdateLoader:false,
                        openEd: false,
                        name: '',
                        uniqueId: '',
                        type: null
                    });
                    this.props.history.push('/products');
                }else if(res.data.statusCode === 0){
                    this.setState({updateError:res.data.error.errorMessage, productUpdateLoader:false})
                }
            })

    }

    render() {

        let { productName, productCode, productASIN, productLink, productDescription, productPrice, productImage, productImagePreview,productUpdateLoader,updateError } = this.state;


        return (
            <HOC>
                {
                    this.state.Loader
                        ? <div className="loader_wrapper"><LoaderSvg /></div>
                        : null
                }
                <div className="body-container-wrapper">
                    <div className="body-container">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
                            <li className="breadcrumb-item"><Link to="/device">Products</Link></li>
                            <li className="breadcrumb-item">Edit Product</li>
                        </ol>
                        <h3 className="text-center mb-4 mt-5">Edit Device</h3>
                        <form onSubmit={this.editProduct} className="edit_device_form mb-5">
                        <h4 className="text-center text-danger mb-4 mt-5">{updateError}</h4>

                            <div className="form-group">
                                <label>Name of Product</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="productName"
                                    id="productName"
                                    value={productName}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className="form-group">
                                <label>Product Code</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="productCode"
                                    id="productCode"
                                    value={productCode}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className="form-group">
                                <label>Product ASIN</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="productASIN"
                                    id="productASIN"
                                    value={productASIN}
                                    onChange={this.handleChange}
                                />
                            </div>



                            <div className="form-group">
                                <label>Product Description</label>
                                <textarea
                                    type="text"
                                    className="form-control"
                                    name="productDescription"
                                    id="productDescription"
                                    value={productDescription}
                                    onChange={this.handleChange}
                                />
                            </div>


                            <div className="form-group">
                                <label>Product Price</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    name="productPrice"
                                    id="productPrice"
                                    value={productPrice}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className="mb-3">
                                <div className="product-img">
                                    <i className="col-img-camera fa fa-camera fa-2x" aria-hidden="true" onClick={(e) => this.productImage.click()}></i>
                                    <img width='620px' src={productImagePreview ? productImagePreview : productImage} className="img-avatar" alt="Avatar" />
                                </div>
                                <input className="img-browse"
                                    ref={(ref) => this.productImage = ref}
                                    type="file"
                                    name="productImage"
                                    id="productImage"
                                    hidden
                                    onChange={this.handleChange}
                                />
                            </div>


                            <div className="form-group">
                                <label>Product Link</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="productLink"
                                    id="productLink"
                                    value={productLink}
                                    onChange={this.handleChange}
                                />
                            </div>


                            <button className="btn btn-primary btn-block mt-4" type="submit" disabled={productUpdateLoader}>
                                {!productUpdateLoader ? <span>Update Product</span> :
                                    <div>
                                        <i className="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i>
                                        <span>Updating....</span>
                                    </div>
                                }
                            </button>

                            {this.state.validateError ? <span className="error">error</span> : null}

                        </form>
                    </div>
                </div>
            </HOC>
        )


    }
}

export default EditProduct;