import React from 'react';
import { NavLink } from 'react-router-dom';

const SideBar = props => {
    return(
        <div className="cm_sidebar">
            <ul>
                <li><NavLink title="Dashboard" to="/dashboard" activeClassName="active"><i className="fa fa-home" ></i> &nbsp;&nbsp; Dashboard</NavLink></li>
                <li><NavLink title="Users" to="/users" activeClassName="active"><i className="fa fa-users" ></i> &nbsp;&nbsp; Users</NavLink></li>
                <li><NavLink title="Device" to="/device" activeClassName="active"><i className="fa fa-cog" ></i> &nbsp;&nbsp; Devices</NavLink></li>
                <li><NavLink title="Coupons" to="/coupons" activeClassName="active"><i className="fa fa-scissors" ></i> &nbsp;&nbsp; Coupons</NavLink></li>
                <li><NavLink title="Affiliation" to="/products" activeClassName="active"><i className="fa fa-shopping-bag" ></i> &nbsp;&nbsp; Affiliation</NavLink></li>
                <li><NavLink title="Dieticians" to="/dieticians" activeClassName="active"><i className="fa fa-user-md"></i> &nbsp;&nbsp; Dieticians</NavLink></li>
                <li><NavLink title="Food items" to="/food-category" activeClassName="active"><i className="fa fa-cubes"></i> &nbsp;&nbsp; Food Categories</NavLink></li>
                <li><NavLink title="Food items" to="/foods" activeClassName="active"><i className="fa fa-cutlery"></i> &nbsp;&nbsp; Food items</NavLink></li>
                <li><NavLink title="Questionaire" to="/questionaire" activeClassName="active"><i className="fa fa-question"></i> &nbsp;&nbsp; Questionnaire</NavLink></li>
                <li><NavLink title="Exercise Category" to="/exercise-category" activeClassName="active"><i className="fa fa-tags"></i> &nbsp;&nbsp; Exercise Category</NavLink></li>
                <li><NavLink title="Exercise" to="/exercise" activeClassName="active"><i className="fa fa-universal-access"></i> &nbsp;&nbsp; Exercise</NavLink></li>
               
            </ul>

            <ul className="cm_admin_links">
                <li><NavLink title="Change Password" to="/change-password" activeClassName="active"><i className="fa fa-key" ></i>&nbsp; Change Password</NavLink></li>
                <li><span title="logout" onClick={props.handlelogOut}><i className="fa fa-sign-out"></i>&nbsp; Logout</span></li>
           </ul>
        </div>
    )
}

export default SideBar;