import React, { Component } from 'react';
import dateFormat from 'dateformat';
import ReactImageFallback from "react-image-fallback";
import HOC from '../../HOC';
import { Link } from 'react-router-dom';
import { getUser } from '../../config/services/usersService';
import { LoaderSvg } from '../Reusable';
import profilePicPlaceholder from '../../assets/images/profilePicPlaceholder.png'

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userId: '',
            profile: {
                weight: {},
                height: {},
                preferences: {},
                beforePic: [],
                afterPic: [],
                dieticianDetails:{}
            },
            loader: true,
        }

        this.getUserProfile = this.getUserProfile.bind(this);
        this.handleViewStats = this.handleViewStats.bind(this);
    }

    getUserProfile() {
        let self =this;
        let id = `?id=${this.props.match.params.id}`;
        getUser(id)
            .then(res => {
                if (res.data.statusCode === 1) {
                    this.setState({
                        profile: res.data.responseData,
                        Loader: false
                    },()=>{
                        const {dieticianDetails} = self.state.profile
                        if(dieticianDetails){
                            if(dieticianDetails.length){
                                self.setState({dieticianDetails:dieticianDetails[0]})
                            }
                        }
                    })
                }
            })
    }

    componentDidMount() {

        let id = this.props.match.params.id;
        this.setState({ userId: id })

        this.getUserProfile();
    }

    handleViewStats() {

    }


    calculateBMI=(e)=>{
        const {weight, height}= this.state.profile

        console.log("===XX>>>", weight, height,e)

        return "Shubham"
    }

    render() {

        let desingCLass = 'shadow-sm p-3 mb-4 rounded';

        const {
            email,
            status,
            countryCode,
            profilePic,
            firstName,
            dob,
            gender,
            lastName,
            mobileNo,
            fitnessCouponCode,
            dietCouponCode,
        } = this.state.profile;

        const{profile,dieticianDetails}=this.state;

        return (
            <HOC>
                <div className="body-container-wrapper">
                    <div className="body-container cm_profile_wrapper cm_profile_wrapper_bg">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
                            <li className="breadcrumb-item"><Link to="/users">Users</Link></li>
                            <li className="breadcrumb-item">profile</li>
                        </ol>

                        {this.state.Loader
                            ? <div className="loader_wrapper"><LoaderSvg /></div>
                            : null}

                        <h4 className="cm_page_heading mb-5">User Profile</h4>
                        <span className={`text-white cm_fixed_btn ${status === 1 ? 'bg-success' : 'bg-danger'}`}>{status === 1 ? 'Active' : 'Inactive'}</span>
                        <div className="row text-center">
                            <div className="col-lg-4 mb-5">
                                <div className="user_profile_img shadow">
                                    <ReactImageFallback
                                        src={profilePic}
                                        fallbackImage={profilePicPlaceholder}
                                    />
                                </div>

                                <Link to={'/chart/' + this.state.userId} className="btn btn-primary mt-4">View Stats</Link>
                                &nbsp;&nbsp;
                                {/* <Link to={'/answers/' + this.state.userId} className="btn btn-primary mt-4">View Answers</Link> */}


                            </div>

                            <div className="col-lg-8 mb-5">
                                <div className="row">
                                    <div className="col-xl-6 cm_right_border">


                                        <h5 className={desingCLass}><b>First Name:</b> {firstName}</h5>
                                        <h5 className={desingCLass}><b>Last Name:</b>{lastName}</h5>
                                        <h5 className={desingCLass}><b>Gender:</b> {gender === 1 ? 'Male' : 'Female'}</h5>
                                        <h5 className={desingCLass}><b>Date of Birth:</b>{dateFormat(dob, "d mmmm yyyy")}</h5>
                                        <h5 className={desingCLass}><b>Mobile Number:</b> {`+${countryCode} - ${mobileNo}`}</h5>
                                        <h5 className={desingCLass}><b>Diet Coupon:</b> {dietCouponCode ? dietCouponCode : 'Not Used yet'}</h5>
                                        <h5 className={desingCLass}><b>Dietician Assigned:</b> {dieticianDetails?dieticianDetails.firstName:'Not assigned yet'} </h5>

                                    </div>

                                    <div className="col-xl-6">
                                        <h5 className={desingCLass}><b>Weight:</b> {`${this.state.profile.weight.value} ${this.state.profile.weight.unit === 1 ? 'Kg' : 'LBS'}`}</h5>
                                        <h5 className={desingCLass}><b>Height:</b> {`${this.state.profile.height.value} ${this.state.profile.height.unit === 1 ? 'Inch' : 'CM'}`}</h5>
                                        <h5 className={desingCLass}><b>Smoker:</b> {this.state.profile.preferences.isSmoker ? 'Yes' : 'No'}</h5>
                                        <h5 className={desingCLass}><b>HyperTension:</b> {this.state.profile.preferences.isHyperTension ? 'Yes' : 'No'}</h5>
                                        <h5 className={desingCLass}><b>Diabetes:</b> {this.state.profile.preferences.isDiabetes ? 'Yes' : 'No'}</h5>
                                        <h5 className={desingCLass}><b>Fitness Coupon:</b> {fitnessCouponCode ? fitnessCouponCode : 'Not Used yet'}</h5>
                                        <h5 className={desingCLass}><b>Email:</b> {email}</h5>

                                    </div>

                                </div>

                                {/* <div className="row mt-1">
                            <div className="col-md-6 mb-5">
                                <h5 className={desingCLass}><b>Before Pic</b></h5>
                                <div className="user_profile_img m-0">
                                
                                <ReactImageFallback
                                    src={this.state.profile.beforePic[0]}
                                    fallbackImage="https://www.mercuregenovasanbiagio.it/upload/home/slides/gym.jpg"
                                />
                                </div>
                            </div>

                            <div className="col-md-6 mb-5">
                                <h5 className={desingCLass}><b>After Pic</b></h5>
                                <div className="user_profile_img m-0" >
                                    <ReactImageFallback
                                        src={this.state.profile.afterPic[0]}
                                        fallbackImage="https://www.mercuregenovasanbiagio.it/upload/home/slides/gym.jpg"
                                    />
                                </div>
                            </div>
                        </div> */}
                            </div>


                        </div>


                    </div>
                </div>
            </HOC>
        );
    }
}

export default Profile;