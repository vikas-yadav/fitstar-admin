import React, { Component } from 'react';
import dateFormat from 'dateformat';
import Pagination from "react-js-pagination";
import HOC from '../../HOC';
import { DeviceService } from '../../config/services/DeviceService';
import { LoaderSvg } from '../Reusable'
import Popup from "reactjs-popup"
import { Link } from 'react-router-dom';

class Device extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNo: 1,
      activePage: 0,
      devices: [],
      Loader: true,
      deviceSearch: '',
      open: false,
      openAddDevicePopup: false,
      openDropdown: false,
      name: '',
      uniqueId: '',
      type: null,
      typeIndex: null,
      errors: false,
      validateError: false,
      deviceObj: '',
      sortField: '',
      sortFieldIcon: false,
      sortType: 2,
      _device: null,
      totalDeviceCount: 0,
      deviceImagePreview: '',
      deviceImage: '',
    }

    this.showDeviceList = this.showDeviceList.bind(this);
    this.handleDeviceSearch = this.handleDeviceSearch.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openDeviceModal = this.openDeviceModal.bind(this);
    this.closeDeviceModal = this.closeDeviceModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleValidetion = this.handleValidetion.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSortList = this.handleSortList.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  handlePageChange(pageNumber) {

    this.setState({ pageNo: pageNumber, Loader: true, deviceSearch: '' }, () => {
      this.showDeviceList();
    });
  }


  openModal(obj) {
    this.setState({
      open: true,
      deviceObj: obj._id,
    })
  }



  closeModal() {
    this.setState({
      open: false,
      device_Id: '',
      deviceImage:'',
      deviceImagePreview:'',
    })
  }



  openDeviceModal() {
    this.setState({ openAddDevicePopup: true })
  }


  closeDeviceModal() {
    this.setState({ openAddDevicePopup: false, deviceImagePreview:'',deviceImage:'' })
  }

  showDeviceList() {
    let params = `?pageNo=${this.state.pageNo - 1}&count=20&search=${this.state.deviceSearch}&sortField=${this.state.sortField}&sortType=${this.state.sortType}`;
    DeviceService.getDeviceList(params)
      .then(res => {
        if (res.data.statusCode === 1) {
          this.setState({
            devices: res.data.responseData.device,
            Loader: false
          })
        }
      })
  }



  handleDeviceSearch(e) {
    this.setState({ deviceSearch: e.target.value }, () => {
      this.showDeviceList();
    });
  }



  componentDidMount() {

    let token = localStorage.getItem('accessToken');

    if (token) {
      this.showDeviceList();
    }
    DeviceService.getTotalDeviceCount()
      .then(res => {
        if (res.data.statusCode === 1) {
          this.setState({ totalDeviceCount: res.data.responseData.deviceCount })
        }
      })
  }



  handleOpenDropdown = () => {
    this.setState({ openDropdown: !this.state.openDropdown })
  }



  getDeiveType = (val) => {
    let typeIndex = ['Application', 'Treadmill', 'Manual Machine', 'Fitness Trackers', 'Smartscale'].indexOf(val)
    this.setState({
      type: val,
      typeIndex: typeIndex,
      openDropdown: !this.state.openDropdown
    })
  }



  handleChange(e) {

    if (e.target.name == 'deviceImage') {

      let file = e.target.files[0];
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          deviceImagePreview: reader.result
        });
      }
      reader.readAsDataURL(file)
      this.setState({
        deviceImagePreview: file
      })
      this.setState({
        deviceImage: e.target.files[0]
      });
    } else {
      this.setState({
        [e.target.name]: e.target.value,
        errors: false
      })
    }
  }

  handleValidetion() {
    let validate = true;
    let name = this.state.name;
    let uniqueId = this.state.uniqueId;

    if (name === '' || name === undefined) {
      validate = false;
    } else {
      validate = true;
    }

    if (uniqueId === '' || uniqueId === undefined) {
      validate = false;
    } else {
      validate = true;
    }

    return validate
  }



  handleSubmit(e) {
    e.preventDefault();
    this.setState({ errors: true })

    let { name, uniqueId, typeIndex, deviceImage } = this.state;
    // let data = `name=${name}&uniqueId=${uniqueId}&type=${typeIndex}`;

    let data = new FormData();

    data.append('name',name);
    data.append('uniqueId', uniqueId);
    data.append('type',typeIndex);
    data.append('deviceImage',deviceImage);

    if (this.handleValidetion()) {

      DeviceService.addDevice(data)
        .then(res => {
          if (res.data.statusCode === 1) {
            this.setState({
              errors: false,
              openAddDevicePopup: false,
              name: '',
              uniqueId: '',
              type: null
            });
            this.showDeviceList();
          }
        })
    } else {
      this.setState({ validateError: true, errors: false })
    }
  }

  handleDelete() {
    let data = `deviceId=${this.state.deviceObj}`;
    DeviceService.DeleteDevice(data)
      .then(res => {
        if (res.data.statusCode === 1) {
          this.setState({
            open: false,
            deviceObj: ''
          })
          this.showDeviceList();
        }
      })
  }


  handleSortList(sortField) {
    const { sortType } = this.state;

    let sortOrder = sortType === 1 ? 2 : 1;

    this.setState({ sortField: sortField, Loader: true, sortType: sortOrder }, () => {
      this.showDeviceList();
      this.setState({
        sortFieldIcon: !this.state.sortFieldIcon
      })
    });
  }





  render() {
    return (
      <HOC>
        <div className="body-container-wrapper">
          <div className="body-container">
            <ol className="breadcrumb">
              <li className="breadcrumb-item"><Link to="/dashboard">Dashboard</Link></li>
              <li className="breadcrumb-item">Device</li>
            </ol>

            {
              this.state.Loader
                ? <div className="loader_wrapper"><LoaderSvg /></div>
                : null
            }

            <div className="users_header">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-6">
                    <h4 className="cm_page_heading">Devices</h4>
                  </div>
                  <div className="col-md-3 text-right cm_search">
                    <input
                      type="text"
                      className="form-control m-b-md-15"
                      placeholder="Search"
                      onChange={this.handleDeviceSearch}
                      value={this.state.deviceSearch}
                    />
                    <i className="fa fa-search"></i>
                  </div>

                  <div className="col-md-3  m-b-md-15">
                    <button onClick={this.openDeviceModal} className="btn btn-primary btn-block">Add Device</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="table-responsive">
              <table className="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col" className="filter_text">Name
                    
                     <i className={`fa fa-sort-alpha-${this.state.sortFieldIcon ? 'desc' : 'asc'}`}></i><button onClick={() => this.handleSortList('name')}></button>
                     
                     </th>
                    <th scope="col">Device Type</th>
                    <th>Device Unique Id</th>
                    <th scope="col" className="filter_text">Created Date <i className={`fa fa-sort-numeric-${this.state.sortFieldIcon ? 'desc' : 'asc'}`}></i><button onClick={() => this.handleSortList('created')}></button></th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>


                <tbody>
                  {
                    this.state.devices ? (
                      this.state.devices.map((device, index) => {
                        return (
                          <tr key={device._id}>
                            <td>{((index + 1) + ((this.state.pageNo - 1) * 20))}</td>
                            <td>{device.name}</td>
                            <td>{['Application', 'Treadmill', 'Manual Machine', 'Fitness Trackers', 'Smartscale'][device.type] ? ['Application', 'Treadmill', 'Manual Machine', 'Fitness Trackers', 'Smartscale'][device.type] : '---------'}</td>
                            <td>{device.uniqueId}</td>
                            <td>{dateFormat(device.created, "d mmmm yyyy")}</td>
                            <td>{device.type == 5 ? <div>Not Editable</div> : <span className="cm_btn_sm" onClick={() => this.openModal(device)}><i className="fa fa-pencil-square" ></i></span>}</td>
                          </tr>
                        )
                      })
                    ) : (
                        <tr>
                          <td colSpan="8">{this.state.error}</td>
                        </tr>
                      )
                  }
                </tbody>


              </table>
              {this.state.devices.length === 0 ? <h3 className="empty_error">Sorry, we couldn't find any content for <span>{this.state.deviceSearch}</span></h3> : null}
              <Pagination
                activePage={this.state.pageNo}
                itemsCountPerPage={20}
                totalItemsCount={this.state.totalDeviceCount}
                pageRangeDisplayed={3}
                onChange={this.handlePageChange}
              />
            </div>


          </div>
        </div>

        <Popup
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div className="cm_modal">
            <span className="cm_modal_close" onClick={this.closeModal}>
              &times;
            </span>
            <h3 className="text-center mb-4">Device Action</h3>
            <div className="btn_group">
              <div className="row">
                <div className="col"><Link to={'/edit-device/' + this.state.deviceObj} className="btn btn-warning btn-sm btn-block">Edit</Link></div>
                <div className="col"><button className="btn btn-danger btn-sm btn-block" onClick={this.handleDelete}>Delete</button></div>
              </div>
            </div>
          </div>
        </Popup>

        <Popup
          open={this.state.openAddDevicePopup}
          closeOnDocumentClick
          onClose={this.closeDeviceModal}
        >
          <div className="cm_modal pl-5 pr-5">
            <span className="cm_modal_close" onClick={this.closeDeviceModal}>
              &times;
            </span>
            <h3 className="text-center mb-4 mt-5">Add New Device</h3>
            <form onSubmit={this.handleSubmit} className="mb-5">

              <div className="form-group">
                <label>Name of device</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="name"
                />
              </div>

              <div className="form-group">
                <label>Unique connection id of device</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                  name="uniqueId"
                />
              </div>

              <div className={this.state.openDropdown ? 'form-group cm_dropdown open' : 'form-group cm_dropdown'}>
                <label>Type of devices</label>
                <span className="form-control cm_blank_input" onClick={this.handleOpenDropdown}>{this.state.type}</span>
                <input type="hidden" value={this.state.devicesType} name="type" />
                <ul>
                  <li onClick={() => this.getDeiveType('Application')}>
                    <h4>Application</h4>
                    <p>Google fit, Samsung Health, Fitbit, Apple Health</p>
                  </li>
                  <li onClick={() => this.getDeiveType('Treadmill')}>
                    <h4>Treadmill</h4>
                    <p>RPM Fitness, Fitkit, ES Linker</p>
                  </li>
                  <li onClick={() => this.getDeiveType('Manual Machine')}>
                    <h4>Manual Machine</h4>
                    <p>RPM Fitness</p>
                  </li>
                  <li onClick={() => this.getDeiveType('Fitness Trackers')}>
                    <h4>Fitness Trackers</h4>
                    <p>Mi Band</p>
                  </li>
                  <li onClick={() => this.getDeiveType('Smartscale')}>
                    <h4>Smartscale</h4>
                    <p>F+</p>
                  </li>
                </ul>
              </div>


              <div className="form-group mt-5 mb-1">
                <label className="d-block">Upload Device Image</label>
                <div className="radio_wrapper text-center">
                  <input className='upload_button' type="file" onChange={this.handleChange} id="deviceImage" name="deviceImage" />
                  <img width="400px" src={this.state.deviceImagePreview} />
                </div>
              </div>

              <button className="btn btn-primary btn-block mt-4">Save</button>
              {this.state.validateError ? <span className="error">error</span> : null}

            </form>
            {
              this.state.errors
                ? <div className="loader_wrapper"><LoaderSvg /></div>
                : null
            }
          </div>
        </Popup>



      </HOC>



    );
  }
}

export default Device;