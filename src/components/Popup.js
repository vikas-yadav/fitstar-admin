import React from 'react';

const Popup = props => {
    let status = props.status ? 'cm_open' : 'cm_close';
    return(
        <div className={`popup_wrapper ${status}`}>
            <div className="popup_inner_wrapper">
                <span className="close_popup" onClick={props.closePop}><i className="fa fa-times-circle-o" aria-hidden="true"></i></span>
                {props.children}
            </div>
        </div>
    )
}

export default Popup;