import setting from './settings'

export default (() => {
  return {
    'AUTH': 'Basic Zml0c3Rhcl9hZG1pbjphZG1pbkBmaXRzdGFy',
    'ADMIN_LOGIN': setting.api.url + "admin/login",
    'ADMIN_LOGOUT': setting.api.url + "user/logout",

    'CHECK_SESSION': setting.api.url + "admin/checkSession",
    'GET_ALL_DATA_COUNT': setting.api.url + "admin/getAllCounts",
    'GET_APP_USERS': setting.api.url + "admin/getAppUsers",
    'GET_USER': setting.api.url + "admin/viewUser",
    'USER_CHANGE_STATUS': setting.api.url + "admin/changeStatus",
    'GET_COUPONS': setting.api.url + "coupon/getAllCoupons",
    'GENERATE_COUPON': setting.api.url + "coupon/generateCoupon",
    'GET_TOTAL_COUPONS': setting.api.url + "coupon/getCouponCount",
    'DELETE_COUPON': setting.api.url + "coupon/deleteCoupon",
    'GET_TOTAL_USERS': setting.api.url + "admin/getUsersCount",
    'GET_TOTAL_DEVICES': setting.api.url + "device/getDeviceCount",
    'SEND_COUPON': setting.api.url + "coupon/sendCoupon",
    'ADMIN_FORGET_PASSWORD': setting.api.url + "admin/forgotPassword",

    'ADD_DIETICIAN': setting.api.url + "admin/addDietician",
    'GET_ALL_DIETICIANS': setting.api.url + "admin/getAllDieticians",
    'ASSIGN_DIETICIAN': setting.api.url + "admin/assignDietician",
    'CHANGE_DIETICIAN_STATUS': setting.api.url + "admin/changeDieticianStatus",

    'GET_EXERCISE_CATEGORIES': setting.api.url + "exerciseCategory/getExerciseCategories",
    'ADD_EXERCISE_CATEGORIES': setting.api.url + "exerciseCategory/addExerciseCategory",
    'EDIT_EXERCISE_CATEGORY': setting.api.url + "exerciseCategory/editExerciseCategory",
    'DELETE_EXERCISE_CATEGORY': setting.api.url + "exerciseCategory/deleteExerciseCategory",

    'GET_FITNESS_EXERCISES': setting.api.url + "fitnessExercise/getFitnessExercises",
    'ADD_FITNESS_EXERCISES': setting.api.url + "fitnessExercise/addFitnessExercise",
    'EDIT_FITNESS_EXERCISE': setting.api.url + "fitnessExercise/editFitnessExercise",
    'DELETE_FITNESS_EXERCISE': setting.api.url + "fitnessExercise/deleteFitnessExercise",
    
    'USER_EMAIL_EXISTS': setting.api.url + "user/emailExists",
    'USER_PHONE_EXISTS': setting.api.url + "user/phoneExists",
    'USER_SEND_OTP': setting.api.url + "user/sendOtp",
    'USER_VERIFY_OTP': setting.api.url + "user/verifyOtp",
    'USER_LOGIN': setting.api.url + "user/login",
    'USER_REGISTER': setting.api.url + "user/`register`",
    'USER_UPDATE_CARD_DETAILS': setting.api.url + "user/updateCardDetails",
    'USER_LOGOUT': setting.api.url + "user/logout",
    'USER_SOCIAL_LOGIN': setting.api.url + "user/socialLogin",
    'USER_FORGET_PASSWORD': setting.api.url + "user/forgetPassword",
    'USER_CHANGE_PASSWORD': setting.api.url + "admin/changePassword",
    'USER_RESET': setting.api.url + "user/reset",
    'USER_GET_PROFILE': setting.api.url + "user/getProfile",
    'USER_EDIT_PROFILE': setting.api.url + "user/editProfile",
    'USER_DELETE_PROFILE': setting.api.url + "user/deleteProfile",
    'USER_SYNCED_DEVICES': setting.api.url + "user/syncedDevices",
    'USER_STATS': setting.api.url + "user/userStats",

    'GET_USER_TARGET': setting.api.url + "target/getUserTargetByAdmin",

    'DEVICE_ADD_DEVICE': setting.api.url + "device/addDevice",
    'DEVICE_UPDATE_DEVICE': setting.api.url + "device/updateDevice",
    'GET_DEVICE': setting.api.url + "device/getDeviceDetails",
    'DEVICE_DELETE_DEVICE': setting.api.url + "device/deleteDevice",
    'DEVICE_GET_DEVICE': setting.api.url + "device/getAllDevices",

    'GET_PRODUCTS': setting.api.url + "products/getAdminProducts",
    'ADD_PRODUCT': setting.api.url + "products/addProduct",
    'DELETE_PRODUCT': setting.api.url + "products/deleteProduct",
    'GET_PRODUCT': setting.api.url + "products/getProduct",
    'EDIT_PRODUCT': setting.api.url + "products/editProduct",
    'GET_PRODUCTS_COUNT': setting.api.url + "products/getTotalProductsCount",

    'GET_ALL_FOODS': setting.api.url + "food/getAllFoods",
    'ADD_FOOD': setting.api.url + "food/addFood",
    'DELETE_FOOD': setting.api.url + "food/deleteFood",
    'EDIT_FOOD': setting.api.url + "food/editFood",
    'UPLOAD_FOOD_FROM_EXCEL': setting.api.url + "food/uploadFoodFromExcel",


    'GET_FOOD_CATEGORIES': setting.api.url + "foodCategory/getFoodCategories",
    'ADD_FOOD_CATEGORY': setting.api.url + "foodCategory/addFoodCategory",
    'EDIT_FOOD_CATEGORY': setting.api.url + "foodCategory/editFoodCategory",
    'DELETE_FOOD_CATEGORY': setting.api.url + "foodCategory/deleteFoodCategory",

    
    'GET_ALL_QUESTIONS': setting.api.url + "questions/getAllQuestionsViaAdmin",
    'CREATE_QUESTION': setting.api.url + "questions/createQuestion",
    'CHANGE_QUESTION_STATUS': setting.api.url + "questions/changeQuestionStatus",

    'VERSION_ADD_APP_VERSION': setting.api.url + "version/addAppVersion",
    'VERSION_ADD_API_VERSION': setting.api.url + "version/addApiVersion",
    'VERSION_DELETE_APP_VERSION': setting.api.url + "version/deleteAppVersion",
    'VERSION_DELETE_API_VERSION': setting.api.url + "version/deleteApiVersion",
    'VERSION_GET_CONFIG_INFO': setting.api.url + "version/getConfigInfo",
  }
})()