import axios from 'axios';
import API from '../Api';

const accessToken = localStorage.getItem('accessToken');

export function getAllCoupon(params) {
    return axios({
        method: 'get',
        url: API.GET_COUPONS + params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function generateCouponService(params) {
    return axios({
        method: 'post',
        url: API.GENERATE_COUPON,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function sendCouponMail(params) {
    return axios({
        method: 'post',
        url: API.SEND_COUPON,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function getAllCouponCount() {
    return axios({
        method: 'get',
        url: API.GET_TOTAL_COUPONS,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function deleteCoupon(params) {
    return axios({
        method: 'post',
        url: API.DELETE_COUPON,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export const CouponService = {
    getAllCoupon,
    generateCouponService,
    getAllCouponCount,
    sendCouponMail,
    deleteCoupon
};