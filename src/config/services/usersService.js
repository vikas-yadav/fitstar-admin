import axios from 'axios';
import API from '../Api';

const accessToken = localStorage.getItem('accessToken');

export function getUserList(params) {
    return axios({
        method: 'get',
        url: API.GET_APP_USERS + params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function getUser(params) {
    return axios({
        method: 'get',
        url: API.GET_USER +'/'+ params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function userChangeStatus(params) {
    return axios({
        method: 'post',
        url: API.USER_CHANGE_STATUS,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

export function getTotalUsersCount() {
    return axios({
        method: 'get',
        url: API.GET_TOTAL_USERS,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,

        }
    })
}

export function getAllDataCount() {
    return axios({
        method: 'get',
        url: API.GET_ALL_DATA_COUNT,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,

        }
    })
}

export function checkSession() {
    return axios({
        method: 'post',
        url: API.CHECK_SESSION,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}


export function userStats(params) {
    return axios({
        method: 'post',
        url: API.USER_STATS,
        data: params,
    })
}

export function getUserTarget(params) {
    return axios({
        method: 'post',
        url: API.GET_USER_TARGET,
        data: params,
    })
}