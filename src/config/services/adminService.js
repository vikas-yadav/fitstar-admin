import axios from 'axios';
import API from '../Api';

export function POST(params) {
    return axios({
        method: 'post',
        url: API.ADMIN_LOGIN,
        data: params,
        headers: {
            Authorization: API.AUTH,
        }
    })
}


export function logout(params) {
    return axios.post(API.ADMIN_LOGOUT, params)
}


export function GET(params) {
    return axios({
        method: 'get',
        url: API.USER_GET_PROFILE,
        data: params.data,
        headers: {
            platform: 3,
            Authorization: API.AUTH,
        }
    })
}

export function PUT(params) {
    return axios({
        method: 'put',
        url: API.USER_EDIT_PROFILE,
        data: params.data,
        headers: {
            platform: 3,
            Authorization: API.AUTH,
        }
    })
}

export function DELETE(params) {
    return axios({
        method: 'delete',
        url: API.USER_DELETE_PROFILE,
        data: params.data,
        headers: {
            platform: 3,
            Authorization: API.AUTH,
        }
    })
}