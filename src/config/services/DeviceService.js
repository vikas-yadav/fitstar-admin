import axios from 'axios';
import API from '../Api';

const accessToken = localStorage.getItem('accessToken');

function getDeviceList(params) {
    return axios({
        method: 'get',
        url: API.DEVICE_GET_DEVICE + params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

function getDeviceOneDevice(params) {
    return axios({
        method: 'get',
        url: API.GET_DEVICE + params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
        }
    })
}

function addDevice(params) {
    return axios({
        method: 'post',
        url: API.DEVICE_ADD_DEVICE,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
           "Content-Type": "application/x-www-form-urlencoded"

        }
    })
}

function DeleteDevice(params) {
    return axios({
        method: 'post',
        url: API.DEVICE_DELETE_DEVICE,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,
           "Content-Type": "application/x-www-form-urlencoded"

        }
    })
}

function UpdateDevice(params) {
    return axios({
        method: 'post',
        url: API.DEVICE_UPDATE_DEVICE,
        data: params,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH
        }
    })
}

function getTotalDeviceCount() {
    return axios({
        method: 'get',
        url: API.GET_TOTAL_DEVICES,
        headers: {
            accessToken: accessToken,
            Authorization: API.AUTH,

        }
    })
}

export const DeviceService = {
    getDeviceList,
    addDevice,
    DeleteDevice,
    UpdateDevice,
    getTotalDeviceCount,
    getDeviceOneDevice
};
