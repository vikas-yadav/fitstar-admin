import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import axios from 'axios';
import { createBrowserHistory } from 'history';

import Login from './components/login/Login';
import ForgetPassword from './components/forgetPassword/ForgetPassword';
import Layout from './components/Layout';

const history = createBrowserHistory();

history.listen((location, action) => {
  window.scrollTo(0, 0)
})


class Routers extends Component {
    render() {
    
    let accessToken = localStorage.getItem('accessToken')
    
    axios.defaults.headers.common['accessToken'] = `${accessToken}`;
    axios.defaults.headers.common['Authorization'] = `${'Basic Zml0c3Rhcl9hZG1pbjphZG1pbkBmaXRzdGFy'}`;

      return (
      <Router history={history}>
        <div className="app">
          <Route path="/login" component={Login} />
          <Route path="/forget-password" component={ForgetPassword} />
          <Route exact={true} path="/" component={Layout} />
          <Route path="/dashboard" component={Layout} />
          <Route path="/users" component={Layout} />
          <Route path="/profile/:id" component={Layout} />
          <Route path="/answers/:id" component={Layout} />
          <Route path="/edit-device/:id" component={Layout} />
          <Route path="/edit-profile" component={Layout} />
          <Route path="/change-password" component={Layout} />
          <Route path="/device" component={Layout} />
          <Route path="/coupons" component={Layout} />
          <Route path="/products" component={Layout} />
          <Route path="/edit-product/:id" component={Layout} />
          <Route path="/chart/:id" component={Layout} />
          <Route path="/dieticians" component={Layout} />
          <Route path="/food-category" component={Layout} />
          <Route path="/foods" component={Layout} />
          <Route path="/questionaire" component={Layout} />
          <Route path="/exercise-category" component={Layout} />
          <Route path="/exercise" component={Layout} />



        </div>
      </Router>
      );
    }
  }
  
  export default Routers;